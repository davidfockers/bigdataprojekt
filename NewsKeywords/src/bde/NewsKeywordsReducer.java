package bde;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class NewsKeywordsReducer extends Reducer<Text, IntWritable, Text, IntWritable>{
	 
	private IntWritable sum = new IntWritable();
		
	 @Override
	  protected void reduce(Text word, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		 int total = 0;
		
		 for (IntWritable val: values){
			 total +=val.get();
		 }
		 sum.set(total);
		 context.write(word, sum);
	 }
}
