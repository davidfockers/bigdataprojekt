package bde;

import json.JSONArray;
import json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.commons.logging.LogFactory;

public class NewsLocationsMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
	
	private final IntWritable intWritable = new IntWritable(1);
	private Text word = new Text();
	private JSONObject object;
	private JSONObject response; 
	private JSONArray docs; 
	private JSONArray keywordsArray;
	private String keyword;
	
	private static final Log LOG = LogFactory.getLog(NewsLocationsMapper.class);
	
	@Override
	protected void map(LongWritable offset, Text quelle, Context context){
		try {

			object = new JSONObject(quelle.toString());			
			response = object.getJSONObject("response");
			docs = response.getJSONArray("docs");
			
			
	        for(int i = 0; i < docs.length(); i++){
	        	keywordsArray = docs.getJSONObject(i).getJSONArray("keywords");
	        	
	        	LOG.info("KeywordsArray: "+keywordsArray.length());
	        	
	        	for(int j=0; j<keywordsArray.length();j++){
	        		LOG.info("Objekt glocations: "+keywordsArray.getJSONObject(j).getString("name"));
	        		if(keywordsArray.getJSONObject(j).getString("name").equals("glocations"))
	        		{
		        		keyword = keywordsArray.getJSONObject(j).getString("value");	
		        		word.set(keyword);
			            context.write(word, intWritable);
	        		}
	        	}
	        }   
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}

