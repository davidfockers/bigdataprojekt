package bde;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.EventDrivenSource;
import org.apache.flume.channel.ChannelProcessor;
import org.apache.flume.conf.Configurable;
import org.apache.flume.event.EventBuilder;
import org.apache.flume.source.AbstractSource;

public class NewsReader extends AbstractSource implements EventDrivenSource,Configurable{
	Timer timer = new Timer();
	@Override
	public void configure(Context arg0) {
		// TODO Auto-generated method stub
	
	}	
	
	
	@Override
	public void start()
	{
		final ChannelProcessor channel = getChannelProcessor(); 
		final Map<String, String> headers = new HashMap<String, String>(); 
		headers.put("timestamp", String.valueOf(new Date().getTime())); 
		
		String ergebnis ="";
		
			try {
				ergebnis = NewsReader.readJsonFromUrl("http://api.nytimes.com/svc/search/v2/articlesearch.json?q=Europe&sort=newest&begin_date=20140201&api-key=6288ddf429eae94b617461044facefca:9:68860898").toString();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Event event = EventBuilder.withBody(ergebnis.getBytes(),headers);
	
		channel.processEvent(event);
		
			try {
				Thread.sleep(100000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  
		timer.schedule(new TimerTask() {
			  @Override
			  public void run() {
				final ChannelProcessor channel = getChannelProcessor(); 
				final Map<String, String> headers = new HashMap<String, String>(); 
				headers.put("timestamp", String.valueOf(new Date().getTime())); 
				
				String ergebnis ="";
				
					try {
						ergebnis = NewsReader.readJsonFromUrl("http://api.nytimes.com/svc/search/v2/articlesearch.json?q=Europe&sort=newest&begin_date=20140201&api-key=6288ddf429eae94b617461044facefca:9:68860898").toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					Event event = EventBuilder.withBody(ergebnis.getBytes(),headers);
			
				channel.processEvent(event);
				
					try {
						Thread.sleep(100000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			  }},
			  3600000);//Jede Stunde die News abfragen
	}
	

	public static String readJsonFromUrl(String urlPath) throws IOException {

			 URL url = new URL(urlPath);
			 URLConnection con = url.openConnection();
			 con.setConnectTimeout(600000);
			 con.setReadTimeout(600000);
			 InputStream is = con.getInputStream();
		    try {
		    	BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		     String jsonText = rd.readLine();
		  
		      return jsonText;
		    } finally {
		      is.close();
		    }
		  }
}
