package bde;


import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;

public class HdfsFile{
	
	//public static void main(String [] args) throws Exception{
		// example usage:
	//	Path myfile = new Path("hdfs://localhost.localdomain:8020/user/cloudera/ausgabe");
	//	List<String> results = readLines(myfile, new Configuration());
	//}
	
	public static List<String> readLines(Path location, Configuration conf) throws Exception {
		FileSystem fileSystem;
	    CompressionCodecFactory factory;
	    FileStatus[] items;
		
		fileSystem = FileSystem.get(location.toUri(), conf);
		factory = new CompressionCodecFactory(conf);
		items = fileSystem.listStatus(location);
		
	   
	    if (items == null) return new ArrayList<String>();
	    
	    List<String> results = new ArrayList<String>();
	   
	    for(FileStatus item: items) {

	      //überflüssige Files ignorieren
	      if(item.getPath().getName().startsWith("_")) {
	        continue;
	      }

	      CompressionCodec codec = factory.getCodec(item.getPath());
	      InputStream stream = null;

	     
	      if (codec != null) {
	        stream = codec.createInputStream(fileSystem.open(item.getPath()));
	      }
	      else {
	        stream = fileSystem.open(item.getPath());
	      }

	      StringWriter writer = new StringWriter();
	      IOUtils.copy(stream, writer, "UTF-8");
	      String raw = writer.toString();
	      String[] resulting = raw.split("\n");
	      for(String str: raw.split("\n")) {
	        results.add(str);
	      }
	    }
	    

	    return results;
	  }	
}
