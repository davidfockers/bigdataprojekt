package bde;
// Copyright 2009 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import com.google.visualization.datasource.DataSourceServlet;
import com.google.visualization.datasource.base.TypeMismatchException;
import com.google.visualization.datasource.datatable.ColumnDescription;
import com.google.visualization.datasource.datatable.DataTable;
import com.google.visualization.datasource.datatable.value.ValueType;
import com.google.visualization.datasource.query.Query;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;


public class LocationsServlet extends DataSourceServlet {

  @Override
  public DataTable generateDataTable(Query query, HttpServletRequest request) {
	  
	int i;  
	List<String> results;
    DataTable data = new DataTable();
    ArrayList<ColumnDescription> cd = new ArrayList<ColumnDescription>();
    
    Path DokumentPfad = new Path("hdfs://localhost.localdomain:8020/user/cloudera/ausgabeNewsLocations");
    
    try {
		results = HdfsFile.readLines(DokumentPfad, new Configuration());
	} catch (Exception e1) {
		e1.printStackTrace();
		results = null;
	}

    cd.add(new ColumnDescription("Location", ValueType.TEXT, "Location"));
    cd.add(new ColumnDescription("Anzahl", ValueType.NUMBER, "Anzahl"));
    
    data.addColumns(cd);
    
    for(i=0;i<results.size();i++)
    {
    	String [] temp = results.get(i).split("\t");
    	try {
    	data.addRowFromValues(temp[0], Integer.valueOf(temp[1]));
    	} catch (TypeMismatchException e) {
    	  System.out.println("Invalid type!");
    	}
    }
    return data;
  }

  @Override
  protected boolean isRestrictedAccessMode() {
    return false;
  }
}
