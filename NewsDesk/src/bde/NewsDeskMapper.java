package bde;

import json.JSONArray;
import json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class NewsDeskMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
	
	private final IntWritable intWritable = new IntWritable(1);
	private Text word = new Text();
	private JSONObject object;
	private JSONObject response; 
	private JSONArray docs; 
	private String newsdesk;
	private String id;
	private static final Log LOG = LogFactory.getLog(NewsDeskMapper.class);
	
	
	@Override
	protected void map(LongWritable offset, Text quelle, Context context){
		try {

			object = new JSONObject(quelle.toString());			
			response = object.getJSONObject("response");
			docs = response.getJSONArray("docs");
			
	        for(int i = 0; i < docs.length(); i++){
	        	id = docs.getJSONObject(i).getString("_id");
	        	newsdesk = docs.getJSONObject(i).getString("news_desk");
	        	word.set(newsdesk);
		        context.write(word, intWritable);
	        }
	           
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}

